package com.rettangolo;

public class Rettangolo {
	private int altezza;
	private int base;
	private int cordinatax;
	private int cordinatay;
	
	public Rettangolo() 	{	
	}
	
	public Rettangolo(int h,int base,int x,int y) {
		altezza=h;
		this.base=base;
		cordinatax=x;
		cordinatay=y;
	}
	
	public int getAltezza() {
		return altezza;
	}
	
	public int getBase() {
		return base;
	}
	
	public int getCordinatax() {
		return cordinatax;
	}
	
	public int getCordinatay() {
		return cordinatay;
	}
	
	public void setAltezza(int h) {
		altezza=h;
	}
	
	public void setBase(int b) {
		base=b;
	}
	
	public void setCordinatax(int x) {
		cordinatax=x;
	}
	
	public void setCordinatay(int y) {
		cordinatay=y;
	}
		
}
