package Banca;

public class ContoBancario {

	 private int soldi;
	 
	 public ContoBancario() {

	 }
	 
	 public ContoBancario(int x) {
		 soldi=x;
	 }
	 
	 public int getSoldi() {
		 return this.soldi;
	 }
 
	 public void setSoldi(int x) {
		 this.soldi=x;
	 }
	 
	 
	 public void preleva(int x) {
		 this.soldi-=x;
	 }
	 
	 public void deposita(int x) {
		 this.soldi+=x;
	 }
	 
	 public void interessi(int x) {
		 this.soldi-=(this.soldi*x)/100;
	 }
}
