package Banca;

public class BancaMain {
	public static void main(String[] arg) {
		ContoBancario harrysChecking= new ContoBancario();
		
		harrysChecking.deposita(1000);
		harrysChecking.preleva(500);
		harrysChecking.preleva(400);
		
		System.out.print("Soldi di harry rimasti "+harrysChecking.getSoldi());
		
		ContoBancario momsSaving= new ContoBancario(1000);
		
		momsSaving.interessi(10);
		
		System.out.print("\nRisparmi di mamma meno gli interessi "+momsSaving.getSoldi());
	}
}
